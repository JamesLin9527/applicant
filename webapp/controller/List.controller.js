sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"nNext/iq/Applicant/formatter/underscore"
], function(Controller, History, underscore) {
	"use strict";

	return Controller.extend("nNext.iq.Applicant.controller.List", {
		onInit: function() {
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this._oRouter.getRoute("List").attachMatched(this._handleRouteMatched, this);
		},
		_handleRouteMatched: function(oEvent) {
			/*
			var sIdentify = oEvent.getParameter("arguments").identify;
			var aFilter = [];
			var ctrl = this;
			$.ajax("/Flow7Api/api/dashboard/processing")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);
					ctrl.getView().setModel(oData, "requisitions");

					aFilter.push(new Filter("Identify", FilterOperator.Contains, sIdentify));
					var oList = ctrl.getView().byId("listPage");

					var oBinding = oList.getBinding("items");
					oBinding.filter(aFilter);
				});
				
				*/
			this.getView().byId("listPage").setTitle(oEvent.getParameter("arguments")["?query"].folderName);

			var folderId = oEvent.getParameter("arguments").folderId;
			var data = this.getOwnerComponent().getModel("diagrams").getData();

			this.getView().setModel(
				new sap.ui.model.json.JSONModel(_.findWhere(data, {
					"FolderGuid": folderId
				}).DiagramLists),
				"lists");
			/*
			console.log(new sap.ui.model.json.JSONModel(_.findWhere(data, {
					"FolderGuid": folderId
				}).DiagramLists),
				"lists");
				*/
		},
		onNavPress: function(oEvent) {
			this._oRouter.navTo("Folder");
			/*		
			var sPreviousHash = History.getInstance().getPreviousHash();

			//The history contains a previous entry
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this._oRouter.navTo("Folder");
			}
			*/
		},

		onItemPress: function(oEvent) {
			var oObject = oEvent.getSource().getBindingContext("lists");
			var oItem = oObject.getModel().getProperty(oObject.getPath());

			this._oRouter.navTo("Form", {
				diagramId: oItem.DiagramId
			});
		}

	});

});