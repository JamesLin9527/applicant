sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageToast"
], function(Controller, JSONModel, Filter, FilterOperator, MessageToast) {
	"use strict";

	return Controller.extend("nNext.iq.Applicant.controller.Fdp", {
		onInit: function() {
			var ctrl = this;
			var self = this;
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this._oRouter.getRoute("Form").attachMatched(this._handleRouteMatched, this);
			//var oModel = this.getView().getModel("fdp");
			//this.getView().setModel(oModel, "fdp");
			// console.log(sText);
			$.ajax("/Flow7Api_CLONING/api/fdp")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);
					ctrl.getView().setModel(oData, "fdp");
					console.log(oData);
				});
		},
		_handleRouteMatched: function(oEvent) {
			this.loadInitialData();
			this.loadErpData();
		},

		loadInitialData: function() {
			var oView = this.getView();

			var oModel =
				(new sap.ui.model.json.JSONModel())
				.attachRequestCompleted(function() {
					oView.setModel(oModel, "fdp");
				})
				.attachPropertyChange(function() {
					sap.ui.getCore().getEventBus().publish("Flow7", "FORM_DATA_CHANGE", oView.getModel("fdp"));
				});
			oModel.loadData("/Flow7Api_CLONING/api/fdp");
		},
		loadErpData: function() {
			var self = this;

			var oView = this.getView();
			oView.setModel(new sap.ui.model.json.JSONModel("/ErpApi_CLONING/api/purma"), "purma");
			
			//oView.setModel(new sap.ui.model.json.JSONModel("/ErpApi_CLONING/api/cmsme"), "cmsme");
			//oView.setModel(new sap.ui.model.json.JSONModel("/ErpApi_CLONING/api/cmsnb"), "cmsnb");
			
			// new sap.ui.model.json.JSONModel("/ErpApi_CLONING/api/receipt").attachRequestCompleted(function(oData) {
			// 	oView.setModel(oData.getSource(), "receiptAll");

			// 	self.renderReceipt(oView.getModel("fdp").getProperty("/DocumentCategoryId"));
			// });
		},
		onPurmaRequest: function() {
			if (!this._oPurmaDialog) {
				this._oPurmaDialog = sap.ui.xmlfragment("nNext.iq.Applicant.view.Purma", this);
				this._oPurmaDialog.setModel(this.getView().getModel("purma"));
			}

			this._oPurmaDialog.open();
		},
		handlePurmaSelection: function(oEvent) {
			var oContexts = oEvent.getParameter("selectedContexts");

			if (oContexts && oContexts.length) {
				var oSelectedItem = oContexts[0].oModel.getProperty(oContexts[0].sPath);

				this.updateFdpData("PaySupplier", oSelectedItem.Ma001);
				this.updateFdpData("PaySupplierName", oSelectedItem.Ma003);
				this.updateFdpData("PaySupplierID", oSelectedItem.Ma005);
				this.updateFdpData("Currency", oSelectedItem.Ma021);
			}
		},
		updateFdpData: function(sKey, sValue) {
			var oModel = this.getView().getModel("fdp");

			oModel.setProperty("/" + sKey, sValue);
			this.getView().setModel(oModel, "fdp");
			
			var ctrl = this;
			var oData = ctrl.getView().getModel("fdp");
			this.publishDataToHost(oData);
		},
		onTextChange: function(oEvent) {
			//處理Model相關資訊，並執行publish
			//var oModel = new sap.ui.model.json.JSONModel("model/Sample.json");
			//sap.ui.getCore().setModel(oModel, "user");
			//this.publishDataToHost(oModel);
			var ctrl = this;
			var oData = ctrl.getView().getModel("fdp");
			console.log(oData);
			this.publishDataToHost(oData);
		},
		publishDataToHost: function(oData) {
			// 收到 submit 通知, 回傳 oData
			sap.ui.getCore().getEventBus().publish("Flow7", "FORM_DATA_CHANGE", oData);
		}
	});
});