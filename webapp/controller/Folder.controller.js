sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/BusyIndicator",
	"sap/ui/core/routing/History"
], function(Controller, BusyIndicator, History) {
	"use strict";

	return Controller.extend("nNext.iq.Applicant.controller.Folder", {
		onInit: function() {
			var self = this;

			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);

			if (History.getInstance().getPreviousHash() === undefined) {
				this._oRouter.navTo("Processing");
			}

			var oModel =
				(new sap.ui.model.json.JSONModel())
				.attachRequestCompleted(function() {
					self.getOwnerComponent().setModel(oModel, "diagrams");

					BusyIndicator.hide();
				});
			oModel.loadData("/Flow7Api_CLONING/api/diagram");
		},
		onFolderPress: function(oEvent) {
			var oObject = oEvent.getSource().getBindingContext("diagrams");
			var oItem = oObject.getModel().getProperty(oObject.getPath());

			this._oRouter.navTo("List", {
				folderId: oItem.FolderGuid,
				query: {
					folderName: oItem.FolderName
				}
			});
		}

	});

});