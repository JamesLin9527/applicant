sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"nNext/iq/Applicant/formatter/Formatter"
], function(Controller,Formatter) {
	"use strict";

	return Controller.extend("nNext.iq.Applicant.controller.Processing", {
		oFormatter: Formatter,
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf nNext.Flow7.Dashboard.Applicant.view.Welcome
		 */
		onInit: function() {
				var self = this;

				new sap.ui.model.json.JSONModel("/Flow7Api_CLONING/api/dashboard/processing")
					.attachRequestCompleted(function(oData) {
						self.getView().setModel(oData.getSource(), "processingLists");
					});
			}
			/**
			 * Called when a controller is instantiated and its View controls (if available) are already created.
			 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
			 * @memberOf nNext.iq.Applicant.view.Processing
			 */
			//	onInit: function() {
			//
			//	},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf nNext.iq.Applicant.view.Processing
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf nNext.iq.Applicant.view.Processing
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf nNext.iq.Applicant.view.Processing
		 */
		//	onExit: function() {
		//
		//	}

	});

});