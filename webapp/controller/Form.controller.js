sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("nNext.iq.Applicant.controller.Form", {
		onInit: function() {
			//使用EventBus接收訂閱者的資訊
			this.oSubmitData = null;
			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.subscribe("Flow7", "FORM_DATA_CHANGE", this.endpointProcess, this);
		},
		endpointProcess: function(sChanel, sEvent, oData) {
			//抓取從訂閱方過來的資料
			this.oSubmitData = oData;
		},
		onSubmit: function() {
			//show出訊息
			var _Jsonshow = "";
			_Jsonshow = this.oSubmitData.getJSON();
			sap.m.MessageToast.show(_Jsonshow);
		}
	});

});